package com.retailer.ws.controllers.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.retailer.client.mdl.dto.Client;
import com.retailer.client.service.IClientService;

@RequestMapping("/client")
@RestController
public class ClientController {

	@Autowired 
	private IClientService clientService;
	
	@PostMapping
	public ResponseEntity<Client> insertClient(@RequestBody Client client) {
		
		try {
			
			return ResponseEntity.ok(this.clientService.insertClient(client));
			
		} catch (Exception e) {
			
			return ResponseEntity.badRequest().build();
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Client> insertClient(@PathVariable Integer id) {
		
		try {
			
			return ResponseEntity.ok(this.clientService.findClient(id));
			
		} catch (Exception e) {
			
			return ResponseEntity.notFound().build();
		}
	}
}
