package com.retailer.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.retailer.client.mdl.dto.Client;
import com.retailer.client.service.IClientService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestClient {
	
	@Autowired
	private IClientService clientService;
	
	@Test
	public void findClientTest() throws Exception {
		
		Client client = clientService.findClient(1);
		assertNotNull(client);
		assertEquals(client.getIdentification(), "1726020074");
	}
	
	@Test
	public void insertClientTest() throws Exception {
		
		Client client = new Client()
				.setBuildingNumber("OE13-90")
				.setCellphone("0999445551")
				.setIdentification("1707366695")
				.setName("Adrian Rojas")
				.setTelephone("023227413")
				.setPrimaryStreet("Valladolid")
				.setSecondaryStreet("Pontevedra");
		
		client = clientService.insertClient(client);
		assertNotNull(client.getId().getClientId());
	}

}
