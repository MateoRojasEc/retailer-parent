package com.retailer.core.operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.retailer.client.mdl.dto.Client;
import com.retailer.client.operator.IClientOperator;
import com.retailer.client.persistance.dao.IClientDAO;


/**
 * @author mateo
 *
 */
@Component
public class ClientOperator implements IClientOperator {

	@Autowired
	private IClientDAO clientDAO;
	
	/**
	 * @see com.retailer.client.operator.IClientOperator#findClient(java.lang.Integer)
	 */
	@Override
	public Client findClient(Integer id) throws Exception {
		
		if(id != null) {
			
			return this.clientDAO.findClient(id);
		}
		
		throw new Exception("ERROR at ClientOperator.findClient() => id cannot be null");
	}

	@Override
	public Client insertClient(Client client) throws Exception {
		return this.clientDAO.insertClient(client);
	}

}
