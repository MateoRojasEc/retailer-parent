package com.retailer.core.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.retailer.client.mdl.dto.Client;
import com.retailer.client.operator.IClientOperator;
import com.retailer.client.service.IClientService;

/**
 * @author mateo
 *
 */
@Transactional
@Service
public class ClientService implements IClientService {

	@Autowired
	private IClientOperator clienteOperator;
	
	@Override
	public Client findClient(Integer id) throws Exception {
		return this.clienteOperator.findClient(id);
	}

	@Override
	public Client insertClient(Client client) throws Exception {
		return this.clienteOperator.insertClient(client);
	}

}
