package com.retailer.core.persistance.dao;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.retailer.client.mdl.dto.Client;
import com.retailer.client.persistance.dao.IClientDAO;

/**
 * @author mateo
 *
 */
@Repository
public class ClientDAO implements IClientDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * @see com.retailer.client.persistance.dao.IClientDAO#findClient(java.lang.Integer)
	 */
	@Override
	public Client findClient(Integer id) throws Exception {
		
		try {
			
			this.sessionFactory.getCurrentSession().clear();
			
			return (Client) sessionFactory.getCurrentSession().createCriteria(Client.class)
					.add(Restrictions.eq("id.clientId", id))
					.uniqueResult();
			
		} catch (HibernateException exception) {
			
			throw new Exception(exception);
		}
	}
	
	@Override
	public Client insertClient(Client client) throws Exception {
		
		try {
			
			this.sessionFactory.getCurrentSession().clear();
			
			sessionFactory.getCurrentSession().saveOrUpdate(client);
			
			return client;
			
		} catch (HibernateException exception) {
			
			throw new Exception(exception);
		}
	}

}
