package com.retailer.client.mdl.dto.id;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * @author mateo
 *
 */
@SuppressWarnings("serial")
@Embeddable
public class ClientID implements Serializable {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_CLIENT")
	private Integer clientId;

	/**
	 * @return
	 */
	public Integer getClientId() {
		return clientId;
	}

	/**
	 * @param id
	 */
	public void setClientId(Integer id) {
		this.clientId = id;
	}
}
