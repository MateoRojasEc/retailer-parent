package com.retailer.client.mdl.dto;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import com.retailer.client.mdl.dto.id.ClientID;

/**
 * @author mateo
 *
 */
@Entity
public class Client {

	@EmbeddedId
	private ClientID id;
	
	private String name;
	
	private String primaryStreet;
	
	private String secondaryStreet;
	
	private String buildingNumber;
	
	private String telephone;
	
	private String cellphone;
	
	private String identification;
	
	public Client() {
		this.id = new ClientID();
	}

	public ClientID getId() {
		return id;
	}

	public void setId(ClientID clientId) {
		this.id = clientId;
	}

	public String getName() {
		return name;
	}

	public Client setName(String name) {
		this.name = name;
		return this;
	}

	public String getPrimaryStreet() {
		return primaryStreet;
	}

	public Client setPrimaryStreet(String primaryStreet) {
		this.primaryStreet = primaryStreet;
		return this;
	}

	public String getSecondaryStreet() {
		return secondaryStreet;
	}

	public Client setSecondaryStreet(String secondaryStreet) {
		this.secondaryStreet = secondaryStreet;
		return this;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public Client setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
		return this;
	}

	public String getTelephone() {
		return telephone;
	}

	public Client setTelephone(String telephone) {
		this.telephone = telephone;
		return this;
	}

	public String getCellphone() {
		return cellphone;
	}

	public Client setCellphone(String cellphone) {
		this.cellphone = cellphone;
		return this;
	}

	public String getIdentification() {
		return identification;
	}

	public Client setIdentification(String identification) {
		this.identification = identification;
		return this;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", primaryStreet=" + primaryStreet + ", secondaryStreet="
				+ secondaryStreet + ", buildingNumber=" + buildingNumber + ", telephone=" + telephone + ", cellphone="
				+ cellphone + ", identification=" + identification + "]";
	}
}
