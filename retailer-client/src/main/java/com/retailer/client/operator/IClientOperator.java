package com.retailer.client.operator;

import org.springframework.stereotype.Component;

import com.retailer.client.mdl.dto.Client;

/**
 * @author mateo
 *
 */
@Component
public interface IClientOperator {
	
	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	Client findClient(Integer id) throws Exception;
	
	/**
	 * @param client
	 * @return
	 * @throws Exception
	 */
	Client insertClient(Client client) throws Exception;
}

