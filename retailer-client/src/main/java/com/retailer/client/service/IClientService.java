package com.retailer.client.service;

import org.springframework.stereotype.Service;

import com.retailer.client.mdl.dto.Client;

/**
 * @author mateo
 *
 */
@Service
public interface IClientService {
	
	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	Client findClient(Integer id) throws Exception;
	
	/**
	 * @param client
	 * @return
	 * @throws Exception
	 */
	Client insertClient(Client client) throws Exception;
}
