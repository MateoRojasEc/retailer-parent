package com.retailer.client.common.factory;

/**
 * @author mateo
 *
 */
public interface BeanName {

	String toString();
}
