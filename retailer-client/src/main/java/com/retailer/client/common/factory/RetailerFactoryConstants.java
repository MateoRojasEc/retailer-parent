package com.retailer.client.common.factory;

/**
 * @author mateo
 *
 */
public class RetailerFactoryConstants {

	enum Service implements BeanName {
		CLIENT_SERVICE("clientService");
		
		private String value;
		
		Service(String value) {
			this.value = value;
		}
		
		@Override
		public String toString() {
			return this.value;
		}
	}
}
