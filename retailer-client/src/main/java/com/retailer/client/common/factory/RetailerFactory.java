package com.retailer.client.common.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.retailer.client.service.IClientService;

/**
 * @author mateo
 *
 */
@Component
public class RetailerFactory {
	
	private static final RetailerFactory INSTANCE;
//	private static final ApplicationContext CONTEXT;
	
	@Autowired
	private ApplicationContext context;
	
	static {
		
		INSTANCE = new RetailerFactory();
//		CONTEXT = new ClassPathXmlApplicationContext();
	}
	
	/**
	 * @return
	 */
//	public IClientService getClientService() {
//		return getBean(CONTEXT, RetailerFactoryConstants.Service.CLIENT_SERVICE, IClientService.class);
//	}
	
	public IClientService getClientService() {
		return context.getBean(IClientService.class);
	}
	
	public static RetailerFactory getInstace() {
		return INSTANCE;
	}
	
	private static <T> T getBean(ApplicationContext context, BeanName beanName, Class<T> beanClass) {
		return context.getBean(beanName.toString(), beanClass);
	}
}
