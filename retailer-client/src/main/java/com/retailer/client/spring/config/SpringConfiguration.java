package com.retailer.client.spring.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@Configuration
public class SpringConfiguration {

	@Bean
	public DataSource dataSource() {
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl("jdbc:mysql://localhost/retailer?serverTimezone=UTC&useSSL=false");
		dataSource.setUsername("retailer");
		dataSource.setPassword("12345678");
		return dataSource;
	}
	
	@Bean
    public LocalSessionFactoryBean sessionFactory() {
    	Properties props = new Properties();
		props.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		props.put("hibernate.show_sql", "true");
		
        final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "com.retailer.client.mdl.dto" });
        sessionFactory.setHibernateProperties(props);

        return sessionFactory;
    }
}
