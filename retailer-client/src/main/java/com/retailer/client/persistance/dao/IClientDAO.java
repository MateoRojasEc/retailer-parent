package com.retailer.client.persistance.dao;

import org.springframework.stereotype.Repository;

import com.retailer.client.mdl.dto.Client;

/**
 * @author mateo
 *
 */
@Repository
public interface IClientDAO {

	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	Client findClient(Integer id) throws Exception;
	
	/**
	 * @param client
	 * @return
	 * @throws Exception
	 */
	Client insertClient(Client client) throws Exception;
}
